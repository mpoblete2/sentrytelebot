// Dependencies are imported.
import * as dotenv from 'dotenv';
import telebot from 'telebot';
import express, {json} from 'express';

dotenv.config();

// Environment variables are declared.
const bot_token = process.env.BOT_TOKEN as string
const app_port = process.env.APP_PORT
const channel_id = process.env.CHANNEL_ID as string
const url = process.env.SENTRY_ISSUE_DETAIL_URL
const app: express.Application = express()

// An instance of the bot is instantiated.
const bot = new telebot({
  token: bot_token,
});

// The Express instance is configured.
app.use(json())
app.get('/', (req,res) => {
  res.status(200).send('🤖 👍');
})

// Emojis are declared for message personalization.
const emojis = {
  info: 'ℹ️',
  error: '⭕️',
  warning: '⚠'
}

// Upon receiving a POST request from the webhook, the data is extracted to populate the message.
app.post('/', async (req,res) => {
  const messageData = {
    env: req.body.event.environment,
    title: req.body.event.title,
    level: req.body.level,
    culprit: req.body.culprit,
    location: req.body.event.location,
    id: req.body.id,
    emoji: ''
  } 
  //console.log(req.body)

// If any null or undefined values are present, they are translated to a more human-readable text.
  if(messageData.location == null){
    messageData.location = 'unknown file path'
  }
  if(messageData.culprit == undefined){
    messageData.culprit = 'Unknown error'
  }
// The appropriate emoji is selected based on the type of incident.
  switch (messageData.level) {
    case 'info':
      messageData.emoji = emojis.info
      break;
    case 'warning':
      messageData.emoji = emojis.warning
      break;
    case 'error':
      messageData.emoji = emojis.error
      break;
    default:
      break;
  }
// The message is generated by filling in the data obtained from the request.
  const message = `${messageData.emoji}<i>(${messageData.level} in ${messageData.env} environment)</i>${messageData.emoji}
${messageData.culprit}
<b>${messageData.title}</b>
<code><i>${messageData.location}</i></code>
<a href="${url}${messageData.id}">view in Sentry</a>`;

// The message is then sent to the channel (channel ID, message, and any additional parameters).
  bot.sendMessage(channel_id, message, {parseMode:'HTML'})
  .catch((e)=> {console.log(e.description)});
})


// WIP: Upon receiving messages in the channel, they will be displayed in the console.
/* bot.on('update', (ctx) => {
  console.log(ctx[0].channel_post);
}) */


// The bot is executed.
bot.start();
// Finally, the application is then executed on the corresponding port.
app.listen(app_port, ()=> {console.log('Listening on port ', app_port)});
